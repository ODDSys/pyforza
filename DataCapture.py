from __future__ import print_function
import socket
import os
import time
import datetime
import sys
import struct
import signal
import sys
import time
import socket
import fcntl
import struct


UDP_PORT = 6496
NetDevice = 'em1'

Running=1
datastep=-1
TransactionCount=0
Connected=0
InMap=0
StepAnimationArray=["|","/","-","\\"]
LastMode=0

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def get_ip_address(ifname):
	try:
		s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		return socket.inet_ntoa(fcntl.ioctl(
			s.fileno(),
			0x8915,  # SIOCGIFADDR
			struct.pack('256s', ifname[:15])
		)[20:24])
	except:
		print ("Device '"+ifname+"' is not available")
		exit()

def progress(count, total, status=''):
	bar_len = 60
	filled_len = int(round(bar_len * count / float(total)))

	percents = round(100.0 * count / float(total), 1)
	bar = bcolors.OKBLUE+bcolors.BOLD+ '=' * filled_len +bcolors.ENDC+'-' * (bar_len - filled_len)
	sys.stdout.write('[%s] %s%s ...%s\r' % (bar, percents, '%', status))
	sys.stdout.flush()

def Accelbox(InX,InY):
	ValX=9+int(InX/5)
	ValY=9+int(InY/2)
	if (ValX>19):
		ValX=19
	if (ValX<0):
		ValX=0
	if (ValY>19):
		ValY=19
	if (ValY<0):
		ValY=0
	intY=-1
	FieldString="";
	while (intY!=19):
		intY+=1;
		if (intY==ValY):
			intX=-1
			while (intX!=19):
				intX+=1
				if (intX==ValX):
					FieldString=FieldString+bcolors.WARNING+bcolors.BOLD+"##"+bcolors.ENDC;
				else:
					FieldString=FieldString+"--";
			FieldString=FieldString+"\r\n"
		else:
			FieldString=FieldString+"----------------------------------------\r\n"
	return FieldString

def signal_handler(sig, frame):
        print('Exiting')
        global Running
        Running=0
        sys.exit(0)

IPAddress=get_ip_address(NetDevice)

timestamp = time.time()
StartTimeStamp = int(round(timestamp * 1000))
st = datetime.datetime.fromtimestamp(timestamp).strftime('%Y-%m-%d %H-%M-%S')

signal.signal(signal.SIGINT, signal_handler)

fileRaw = open("Forza Raw "+st+".txt","w") 
fileCSV = open("Forza "+st+".csv","w") 
fileCSV.write ("#,TimeStamp,MaxRPM,IdleRPM,CurRPM,VelocityX,VelocityY,VelocityZ,AccelerationX,AccelerationY,AccelerationZ,Yaw,Pitch,Roll\r\n")

sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM) # UDP
sock.bind((IPAddress, UDP_PORT))

print ("IP:"+IPAddress+"  Port:"+str(UDP_PORT)+"  NetDevice:"+NetDevice)
print ("Files: 'Forza Raw "+st+".txt' and 'Forza "+st+".csv'")
print ("")
print ("")
print ("")
print ("")
print ("")
print ("")
print ("")
print ("")
print ("")
print ("")
print ("")
print ("")
print ("")
print ("")
print ("")
print ("")
print ("")
print ("")
print ("")
print ("")
print ("")
print ("")
print ("")
print ("")
print ("")
print ("")
while Running==1:
	data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
	fileRaw.write (data);
	if ord(data[0])==1:
		InMap=1
	else:
		InMap=0

	RPMMax=0
	RPMIdle=0
	RPMCurrent=0
	VelocityX=0
	VelocityY=0
	VelocityZ=0
	
	StepText="-";
	
	Bytes_RPMMax=[]
	Bytes_RPMMIdle=[]
	Bytes_RPMCur=[]
	
	Bytes_AccelX=[]
	Bytes_AccelY=[]
	Bytes_AccelZ=[]

	Bytes_VelocityX=[]
	Bytes_VelocityY=[]
	Bytes_VelocityZ=[]
	
	Bytes_AVelocityX=[]
	Bytes_AVelocityY=[]
	Bytes_AVelocityZ=[]
	
	Bytes_Yaw=[]
	Bytes_Pitch=[]
	Bytes_Roll=[]
		
	if (InMap==1 and (len(data)>=72)):
		TransactionCount=TransactionCount+1
		
		Bytes_RPMMax=[ord(data[8]), ord(data[9]), ord(data[10]), ord(data[11])]
		Bytes_RPMMIdle=[ord(data[12]), ord(data[13]), ord(data[14]), ord(data[15])]
		Bytes_RPMCur=[ord(data[16]), ord(data[17]), ord(data[18]), ord(data[19])]
		
		Bytes_AccelX=[ord(data[20]), ord(data[21]), ord(data[22]), ord(data[23])]
		Bytes_AccelY=[ord(data[24]), ord(data[25]), ord(data[26]), ord(data[27])]
		Bytes_AccelZ=[ord(data[28]), ord(data[29]), ord(data[30]), ord(data[31])]

		Bytes_VelocityX=[ord(data[31]), ord(data[32]), ord(data[33]), ord(data[34])]
		Bytes_VelocityY=[ord(data[35]), ord(data[36]), ord(data[37]), ord(data[38])]
		Bytes_VelocityZ=[ord(data[39]), ord(data[40]), ord(data[41]), ord(data[42])]
		
		Bytes_AVelocityX=[ord(data[43]), ord(data[44]), ord(data[45]), ord(data[46])]
		Bytes_AVelocityY=[ord(data[47]), ord(data[48]), ord(data[49]), ord(data[50])]
		Bytes_AVelocityZ=[ord(data[51]), ord(data[52]), ord(data[53]), ord(data[54])]
		
		Bytes_Yaw=[ord(data[55]), ord(data[56]), ord(data[57]), ord(data[58])]
		Bytes_Pitch=[ord(data[59]), ord(data[60]), ord(data[61]), ord(data[62])]
		Bytes_Roll=[ord(data[63]), ord(data[64]), ord(data[65]), ord(data[66])]

		aa1=str(bytearray(Bytes_RPMMax))
		RPMMax=struct.unpack('<f', aa1)[0]

		aa2=str(bytearray(Bytes_RPMMIdle))
		RPMIdle=struct.unpack('<f', aa2)[0]

		aa3=str(bytearray(Bytes_RPMCur))
		RPMCurrent=struct.unpack('<f', aa3)[0]
		
		aa4=str(bytearray(Bytes_AccelX))
		AccelX=struct.unpack('<f', aa4)[0]

		aa5=str(bytearray(Bytes_AccelY))
		AccelY=struct.unpack('<f', aa5)[0]

		aa6=str(bytearray(Bytes_AccelZ))
		AccelZ=struct.unpack('<f', aa6)[0]
		
		aa7=str(bytearray(Bytes_VelocityX))
		VelocityX=struct.unpack('<f', aa7)[0]

		aa8=str(bytearray(Bytes_VelocityY))
		VelocityY=struct.unpack('<f', aa8)[0]

		aa9=str(bytearray(Bytes_VelocityZ))
		VelocityZ=struct.unpack('<f', aa9)[0]
		
		aa10=str(bytearray(Bytes_Yaw))
		Yaw=struct.unpack('<f', aa10)[0]

		aa11=str(bytearray(Bytes_Pitch))
		Pitch=struct.unpack('<f', aa11)[0]

		aa12=str(bytearray(Bytes_Roll))
		Roll=struct.unpack('<f', aa12)[0]
		
		SampleTimeStamp = (int(round(time.time() * 1000)))-StartTimeStamp
		fileCSV.write(str(TransactionCount)+","+str(SampleTimeStamp)+","+str(RPMMax)+","+str(RPMIdle)+","+str(RPMCurrent)+","+str(VelocityX)+","+str(VelocityY)+","+str(VelocityZ)+","+str(AccelX)+","+str(AccelY)+","+str(AccelZ)+","+str(Yaw)+","+str(Pitch)+","+str(Roll)+"\r\n");
		
		
	datastep=datastep+1;		
	StepText=StepAnimationArray[datastep]
	if (datastep==3):
		datastep=-1
	
	
	if (RPMMax!=0):
		print ("\033[F\033[F\033[F\033[F\033[F\033[F\033[F\033[F\033[F\033[F\033[F\033[F\033[F\033[F\033[F\033[F\033[F\033[F\033[F\033[F\033[F\033[F\033[F\033[F\033[F\033[F"+bcolors.HEADER+StepText+" "+bcolors.OKBLUE+"In Race"+bcolors.ENDC+"               ")
		progress (RPMCurrent,RPMMax,"RPM "+bcolors.OKGREEN+bcolors.BOLD+str(int(RPMCurrent)).rjust(4)+bcolors.ENDC+"/"+bcolors.FAIL+str(int(RPMMax))+bcolors.ENDC+"   ")
		print ("")
		progress (100-AccelX,200,"Acceleration X "+bcolors.OKGREEN+bcolors.BOLD+str(round(AccelX,3))+bcolors.ENDC+"   ")
		print ("")
		progress (100-AccelY,200,"Acceleration Y "+bcolors.OKGREEN+bcolors.BOLD+str(round(AccelY,3))+bcolors.ENDC+"   ")
		print ("")
		progress (100-AccelZ,200,"Acceleration Y "+bcolors.OKGREEN+bcolors.BOLD+str(round(AccelZ,3))+bcolors.ENDC+"   ")
		print ("")
		AccelString=Accelbox (AccelX,AccelZ)
		print (AccelString)


		
	else:
		print ("\033[F\033[F\033[F\033[F\033[F\033[F\033[F\033[F\033[F\033[F\033[F\033[F\033[F\033[F\033[F\033[F\033[F\033[F\033[F\033[F\033[F\033[F\033[F\033[F\033[F\033[F"+bcolors.HEADER+StepText+" "+bcolors.WARNING+"Menu/Pause/Rewind"+bcolors.ENDC+"                                                                     ")
		print ("                                                                                                          ")
		print ("                                                                                                          ")
		print ("                                                                                                          ")
		print ("                                                                                                          ")
		print ("                                                                                                          ")
		print ("                                                                                                          ")
		print ("                                                                                                          ")
		print ("                                                                                                          ")
		print ("                                                                                                          ")
		print ("                                                                                                          ")
		print ("                                                                                                          ")
		print ("                                                                                                          ")
		print ("                                                                                                          ")
		print ("                                                                                                          ")
		print ("                                                                                                          ")
		print ("                                                                                                          ")
		print ("                                                                                                          ")
		print ("                                                                                                          ")
		print ("                                                                                                          ")
		print ("                                                                                                          ")
		print ("                                                                                                          ")
		
		print ("                                                                                                          ")
		print ("                                                                                                          ")
		print ("                                                                                                          ")
		
		print ("                                                                                                          ")

		print ("                                                                                                          ", end='')

fileRaw.close()
fileCSV.close()
