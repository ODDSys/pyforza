# PyForza

The purpose of PyForza is to enable the capture of vehicle data which can be streamed via UDB to a remote machine.

Currently there is a few utlilities available to see the data but nothing that records it to a file which can be reviewed later in Excel/Libreoffice Calc.

# Environment Suggestions

## Network
We recommend using ethernet for both the game and listening computer as it is less susceptible to interference like WIFI.

## Sample size
The capture can generate very large files which can be difficult to process in some commercial spread sheet applications, if you do experience this issue you can:

1. Use an open source Spreadsheet application like LibreOffice as it can handle large files
2. Record smaller samples, for example stop and start the script between each race
3. Import the data into a database application, for example MySQL for further processing

# Requirements

* Python 2.7
* Text editor
* Basic command line experience

# Setup PyForza

PyForza currently only runs on linux as I am using linux paths for the network device (/dev/em1) but this can be easliy modified for a windows system.

You can update the Network Device path by opening the DataCapture.py file and updating variable “NetDevice” as shown here.

<a href="" target="_blank"><img src="https://gitlab.com/ODDSys/pyforza/wikis/uploads/972f8955f7a0c771ced225a5f32c9258/image.png"/></a>


# Run PyForza

After you have entered your computers network device path you can launch PyForza by browsing to the folder you cloned the repos to in command line then enter “python DataCapture.py”.

<a href="" target="_blank"><img src="https://gitlab.com/ODDSys/pyforza/wikis/uploads/b9f7405083f2e4ea4aa01968f2fe2279/image.png"/></a>

PyForza will display the IP and Port for the machine which is listening for Forza data packets, please update the setting in Forza to match the IP and port number.

The script will wait until it see packets of data after which it will display RPM and Acceleration data while recording the following data to a CSV file which is shown after PyForza has started

* Packet Number
* TimeStamp
* MaxRPM
* IdleRPM
* CurRPM
* VelocityZ
* VelocityX
* VelocityY
* AccelerationX
* AccelerationY
* AccelerationZ
* Yaw
* Pitch
* Roll

# Exit PyForza

CTRL+C will exit and close open files


# Live Data

PyForza will display live data while running in command line.

Images to be added to Readme.


# Need to do

1. Add the rest of the data available in the packet
2. Add stream repeater out to another listening client (repeater)


# Other

If you have any suggestions please contact me to be added to the project.
There is some stupid things in the code (like prints clearing text) that will be cleaned up as soon as the core stuff is sorted.